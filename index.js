// Bài 1: Tính tiền lương nhân viên
var tinhTienLuong = document.getElementById("tinhTienLuong");
tinhTienLuong.onclick = function tinhTien() {
  // Lấy thông tin từ ô input
  var luongMotNgay = document.getElementById("txtLuongMotNgay").value;
  var soNgayLam = document.getElementById("txtSoNgayLam").value;
  // tạo biến chứa tiền lương tính được
  var tienLuong = 0;
  // tính lương
  tienLuong = luongMotNgay * soNgayLam;
  // Show thông tin ra ngoài giao diện
  document.getElementById("result1").innerHTML =
    " Tổng tiền lương = " + tienLuong.toLocaleString() + " VND";
};

// Bài 2: Tính giá trị trung bình
var tinhTrungBinh = document.getElementById("tinhTrungBinh");
tinhTrungBinh.onclick = function tinhTrungBinh() {
  // Lấy thông tin từ ô input
  var num1 = document.getElementById("txtNum1").value * 1;
  var num2 = document.getElementById("txtNum2").value * 1;
  var num3 = document.getElementById("txtNum3").value * 1;
  var num4 = document.getElementById("txtNum4").value * 1;
  var num5 = document.getElementById("txtNum5").value * 1;
  //   tạo biến chứa giá trị trung bình
  var gttb = 0;
  // Tính trung bình
  gttb = (num1 + num2 + num3 + num4 + num5) / 5;
  //   Show gttb tính được ra ngoài giao diện
  document.getElementById("result2").innerHTML = "GTTB = " + gttb;
};

// Bài 3: quy đổi tiền
var quyDoiTien = document.getElementById("quyDoi");
quyDoiTien.onclick = function quyDoiTien() {
  // Lấy thông tin ở ô input
  var soUSD = document.getElementById("txtUSD").value * 1;
  // Tạo biến chứa số tiền sau quy đổi
  var doiTien = 0;
  // tính tiền quy đổi
  doiTien = soUSD * 23500;
  // show số tiền đã quy đổi lên giao diện
  document.getElementById("result3").innerHTML =
    "Số tiền đổi được là " + doiTien.toLocaleString() + " VND";
};

// Bài 4: Tính chu vi, diện tích hình chữ nhật
var tinhCongThuc = document.getElementById("tinh");
tinhCongThuc.onclick = function tinhCongThuc() {
  // lấy thông tin từ ô input
  var chieuDai = document.getElementById("txtChieuDai").value * 1;
  var chieuRong = document.getElementById("txtChieuRong").value * 1;
  // tạo biến chứa chu vi, diện tích
  var dienTich = 0;
  var chuVi = 0;
  // tính
  dienTich = chieuDai * chieuRong;
  chuVi = (chieuDai + chieuRong) * 2;
  // show kết quả tính được ra giao diện
  document.getElementById("result4").innerHTML =
    "Diện tích = " + dienTich + " ;" + "  Chu vi = " + chuVi;
};

// Bài 5: Tính tổng 2 ký số
var tinhKySo = document.getElementById("tongKySo");
tinhKySo.onclick = function tinhKySo() {
  // lấy thông tin ô input
  var txtSo = document.getElementById("txtSo").value * 1;
  // tạo biến chứa só hàng chục, số đơn vị
  var hangChuc = null;
  var donVi = null;
  // tách số
  hangChuc = Math.floor(txtSo / 10);
  donVi = Math.floor(txtSo % 10);

  //   tạo biến chứa tổng 2 ký số
  var tongKySo = 0;

  // tính
  tongKySo = hangChuc + donVi;
  // show kết quả
  document.getElementById("result5").innerHTML = "Tổng ký số = " + tongKySo;
};

